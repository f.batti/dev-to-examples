# What is this Repo

In this repository I am making examples of how to use GitLab's CI/CD pipelines. Every folder is a article I released on dev.to.
Inside the current CI/CD file is always the latest code. If you are interested in a particular set up go inside the folder and see the code.

## Articles

- https://dev.to/snakepy/5-things-i-wished-i-had-known-when-i-started-to-maintain-gitlab-cicd-1e4i
